//javascript
const path = require('path');

module.exports = {
  devtool: 'eval-source-map',
  entry: {
    main: './src/index.js'
  },
  
  mode: 'development',
  
  module: {
    rules: [{
      exclude: /node_modules/,
      use:
      [{
        loader: 'babel-loader'
      }],
      test: /\.jsx?$/
    }]
  },
  
  output: {
    path: __dirname + '/dist',
    filename: '[name].bundle.js',
  },
};